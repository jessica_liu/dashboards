/*
 * @Author: Melanie
 * @Date: 2022-09-23 11:04:33
 * @LastEditTime: 2022-09-23 11:18:21
 * @LastEditors: Melanie
 * @Descripttion: 
 */
const express = require('express');
const path = require('path');
const app = express();

//可以访问所有文件
app.use(express.static('./'));

app.listen(9100, () => {
  console.log('App listening at port 9100');
});