var setJiaDonglv = function () {
  var jiadonglv = echarts.init(document.getElementById('jiadonglv'))
  var option
  option = {
    // backgroundColor: '#080b30',
    title: {
      text: '机台稼动率趋势图',
      textStyle: {
        align: 'center',
        color: '#fff',
        fontSize: 20
      },
      top: '0',
      left: 'center'
    },
    legend: { type: 'plain', show: true, bottom: '3%', left: 'center', textStyle: { color: '#fff' } },
    grid: {
      left: '5%',
      right: '5%',
      top: '12%',
      bottom: '12%',
      containLabel: true
    },
    xAxis: [
      {
        type: 'category',
        axisLine: {
          show: true
        },
        splitArea: {
          // show: true,
          color: '#fff',
          lineStyle: {
            color: '#fff'
          }
        },
        axisLabel: {
          color: '#fff'
        },
        splitLine: {
          show: false
        },
        boundaryGap: false,
        data: ['A', 'B', 'C', 'D', 'E', 'F']
      }
    ],

    yAxis: [
      {
        type: 'value',
        min: 0,
        // max: 140,
        splitNumber: 4,
        splitLine: {
          show: true,
          lineStyle: {
            color: 'rgba(255,255,255,0.1)'
          }
        },
        axisLine: {
          show: true
        },
        axisLabel: {
          show: true,
          margin: 20,
          textStyle: {
            color: '#d1e6eb'
          }
        },
        axisTick: {
          show: false
        }
      }
    ],
    series: [
      {
        name: '注册总量1',
        type: 'line',
        smooth: true, //是否平滑
        showAllSymbol: true,
        // symbol: 'image://./static/images/guang-circle.png',
        symbol: 'circle',
        symbolSize: 2,
        lineStyle: {
          normal: {
            color: '#6c50f3'
          }
        },
        label: {
          show: true,
          position: 'top',
          textStyle: {
            color: '#6c50f3'
          }
        },
        itemStyle: {
          color: '#6c50f3',
          borderColor: '#fff',
          borderWidth: 3
        },
        tooltip: {
          show: false
        },
        areaStyle: {
          normal: {
            color: {
              type: 'linear',
              x: 0,
              y: 0,
              x2: 0,
              y2: 1,
              colorStops: [
                {
                  offset: 0,
                  color: 'rgba(108,80,243,0.3)' // 0% 处的颜色
                },
                {
                  offset: 1,
                  color: 'rgba(108,80,243,0)' // 100% 处的颜色
                }
              ],
              global: false // 缺省为 false
            }
          }
        },
        data: [502.84, 205.97, 332.79, 281.55, 398.35, 214.02]
      },
      {
        name: '注册总量2',
        type: 'line',
        smooth: true, //是否平滑
        showAllSymbol: true,
        // symbol: 'image://./static/images/guang-circle.png',
        symbol: 'circle',
        symbolSize: 2,
        lineStyle: {
          normal: {
            color: '#00ca95'
          }
        },
        label: {
          show: true,
          position: 'top',
          textStyle: {
            color: '#00ca95'
          }
        },

        itemStyle: {
          color: '#00ca95',
          borderColor: '#fff',
          borderWidth: 3
        },
        tooltip: {
          show: false
        },
        areaStyle: {
          normal: {
            color: {
              type: 'linear',
              x: 0,
              y: 0,
              x2: 0,
              y2: 1,
              colorStops: [
                {
                  offset: 0,
                  color: 'rgba(0,202,149,0.3)' // 0% 处的颜色
                },
                {
                  offset: 1,
                  color: 'rgba(0,202,149,0)' // 100% 处的颜色
                }
              ],
              global: false // 缺省为 false
            }
          }
        },
        data: [281.55, 398.35, 214.02, 179.55, 289.57, 356.14]
      }
    ]
  }

  option && jiadonglv.setOption(option)
}
var setErrorLine = function () {
  var jiadonglv = echarts.init(document.getElementById('error'))
  var option

  option = {
    // backgroundColor: '#080b30',
    title: {
      text: '异常数量趋势图',
      textStyle: {
        align: 'center',
        color: '#fff',
        fontSize: 20
      },
      top: '0',
      left: 'center'
    },
    legend: { type: 'plain', show: true, bottom: '3%', left: 'center', textStyle: { color: '#fff' } },
    grid: {
      left: '5%',
      right: '5%',
      top: '12%',
      bottom: '12%',
      containLabel: true
    },
    xAxis: [
      {
        type: 'category',
        axisLine: {
          show: true
        },
        splitArea: {
          // show: true,
          color: '#fff',
          lineStyle: {
            color: '#fff'
          }
        },
        axisLabel: {
          color: '#fff'
        },
        splitLine: {
          show: false
        },
        boundaryGap: false,
        data: ['11-19', '11-20', '11-21', '11-22', '11-23', '11-24']
      }
    ],

    yAxis: [
      {
        type: 'value',
        min: 0,
        // max: 140,
        splitNumber: 4,
        splitLine: {
          show: true,
          lineStyle: {
            color: 'rgba(255,255,255,0.1)'
          }
        },
        axisLine: {
          show: true
        },
        axisLabel: {
          show: true,
          margin: 20,
          textStyle: {
            color: '#d1e6eb'
          }
        },
        axisTick: {
          show: false
        }
      }
    ],
    series: [
      {
        name: '错误量',
        type: 'line',
        smooth: true, //是否平滑
        showAllSymbol: true,
        // symbol: 'image://./static/images/guang-circle.png',
        symbol: 'circle',
        symbolSize: 2,
        lineStyle: {
          normal: {
            color: '#D60040'
          }
        },
        label: {
          show: true,
          position: 'top',
          textStyle: {
            color: '#D60040'
          }
        },
        itemStyle: {
          color: '#D60040',
          borderColor: '#fff',
          borderWidth: 3
        },
        tooltip: {
          show: false
        },
        areaStyle: {
          normal: {
            color: {
              type: 'linear',
              x: 0,
              y: 0,
              x2: 0,
              y2: 1,
              colorStops: [
                {
                  offset: 0,
                  color: 'rgba(214,0,64,0.3)' // 0% 处的颜色
                },
                {
                  offset: 1,
                  color: 'rgba(214,0,64,0)' // 100% 处的颜色
                }
              ],
              global: false // 缺省为 false
            }
          }
        },
        data: [502.84, 205.97, 332.79, 281.55, 398.35, 214.02]
      }
    ]
  }

  option && jiadonglv.setOption(option)
}
var setDoneRate = function () {
  var jiadonglv = echarts.init(document.getElementById('donerate'))
  var option
  option = {
    title: {
      text: '完工准点率趋势图',
      textStyle: {
        align: 'center',
        color: '#fff',
        fontSize: 20
      },
      top: '0',
      left: 'center'
    },
    legend: { type: 'plain', show: true, bottom: '3%', left: 'center', textStyle: { color: '#fff' } },
    grid: {
      left: '5%',
      right: '5%',
      top: '12%',
      bottom: '12%',
      containLabel: true
    },
    xAxis: [
      {
        type: 'category',
        axisLine: {
          show: true
        },
        splitArea: {
          // show: true,
          color: '#fff',
          lineStyle: {
            color: '#fff'
          }
        },
        axisLabel: {
          color: '#fff'
        },
        splitLine: {
          show: false
        },
        boundaryGap: false,
        data: ['11-19', '11-20', '11-21', '11-22', '11-23', '11-24']
      }
    ],

    yAxis: [
      {
        type: 'value',
        min: 0,
        // max: 140,
        splitNumber: 4,
        splitLine: {
          show: true,
          lineStyle: {
            color: 'rgba(255,255,255,0.1)'
          }
        },
        axisLine: {
          show: true
        },
        axisLabel: {
          show: true,
          margin: 20,
          textStyle: {
            color: '#d1e6eb'
          }
        },
        axisTick: {
          show: false
        }
      }
    ],
    series: [
      {
        name: '错误量',
        type: 'line',
        smooth: true, //是否平滑
        showAllSymbol: true,
        // symbol: 'image://./static/images/guang-circle.png',
        symbol: 'circle',
        symbolSize: 2,
        lineStyle: {
          normal: {
            color: '#00ca95'
          }
        },
        label: {
          show: true,
          position: 'top',
          textStyle: {
            color: '#00ca95'
          }
        },
        itemStyle: {
          color: '#00ca95',
          borderColor: '#fff',
          borderWidth: 3
        },
        tooltip: {
          show: false
        },
        areaStyle: {
          normal: {
            color: {
              type: 'linear',
              x: 0,
              y: 0,
              x2: 0,
              y2: 1,
              colorStops: [
                {
                  offset: 0,
                  color: 'rgba(108,80,243,0.3)' // 0% 处的颜色
                },
                {
                  offset: 1,
                  color: 'rgba(108,80,243,0)' // 100% 处的颜色
                }
              ],
              global: false // 缺省为 false
            }
          }
        },
        data: [502.84, 205.97, 332.79, 281.55, 398.35, 214.02]
      }
    ]
  }

  option && jiadonglv.setOption(option)
}
// <li class="pulll_left barsplit"><span style="color:rgba(133,161,187,0.9)">@Model.ToDoCount</span>
// </<li class="pulll_left barsplit"><span style="color:rgba(0,254,129,0.8)">@Model.DoneCount</span></li>
// <li class="pulll_left barsplit"><span style="color:rgba(255,128,129,0.9)">@Model.DelayCount</span></li>
var setTimesPie = function () {
  var jiadonglv = echarts.init(document.getElementById('timespie'))
  var option
  var seriesData = [
    {
      name: 'CNC',
      value: '40000'
    },
    {
      name: 'WEDM',
      value: '40000'
    },
    {
      name: 'EDM',
      value: '53000'
    }
  ]
  var colorList = ['#6c50f3', '#D60040', '#00ca95', '#FDD56A', '#FDB36A', '#FD866A', '#9E87FF']
  option = {
    title: {
      text: '工时消耗分布图',
      top: '2%',
      left: 'center',
      textStyle: {
        color: '#fff'
      }
    },
    legend: {
      show: true,
      type: 'scroll',
      orient: 'horizontal',
      left: 'center',
      align: 'auto',
      bottom: '0%',
      textStyle: {
        color: '#fff'
      }
    },
    series: [
      {
        type: 'pie',
        z: 3,
        center: ['50%', '50%'],
        radius: ['25%', '45%'],
        clockwise: true,
        avoidLabelOverlap: true,
        hoverOffset: 15,
        itemStyle: {
          normal: {
            color: function (params) {
              return colorList[params.dataIndex]
            }
          }
        },
        label: {
          show: true,
          position: 'outside',
          formatter: '{a|{b}：{d}%}\n{hr|}',
          rich: {
            hr: {
              backgroundColor: 't',
              borderRadius: 3,
              width: 3,
              height: 3,
              padding: [3, 3, 0, -12]
            },
            a: {
              padding: [-30, 15, -20, 15]
            }
          }
        },
        labelLine: {
          normal: {
            length: 20,
            length2: 30,
            lineStyle: {
              width: 1
            }
          }
        },
        data: seriesData
      }
    ]
  }
  option && jiadonglv.setOption(option)
}
